# InfluxDB Kurzdoku

### Umzug InfluxDB-Datenbank auf anderen Rechner
Wenn alle Datenbanken umgezogen werden sollen am Besten einfach die Daten aus dem InfluxDB-Volume vom Quellrechner auf den Zielrechner kopieren.

#### Vergessenes Admin-Passwort zurücksetzen
1. InfluxDB-Authentifizierung ausschalten - dann wird die Benutzeranmeldung nicht mehr geprüft und man ist immer Admin: Dazu im docker-compose.yml die Zeile "INFLUXDB_HTTP_AUTH_ENABLED=true" auf "false" setzen
2. InfluxDB-Container neu starten: `docker-compose stop influxdb && docker-compose start influxdb`
3. `docker exec -ti influxdb bash`
4. `influx`
5. `SET PASSWORD FOR admin = '<neuesPasswort>'`
6. `exit`
7. Im docker-compose.yml "INFLUXDB_HTTP_AUTH_ENABLED" wieder auf "true" setzen
8. InfluxDB-Container neu starten: `docker-compose stop influxdb && docker-compose start influxdb`

#### Benutzerverwaltung
Die Benutzerverwaltung erfolgt im Docker-Container (`docker exec -ti influxdb bash`)

Bei eingeschalteter Authentifizierung (siehe oben) kann nur der User "admin" Benutzer anlegen, Rechte verwalten und Passwörter ändern.
`influx -username admin -password '<adminpasswort>'` 

Mehr Doku: https://influxdbcom.readthedocs.io/en/latest/content/docs/v0.9/administration/administration/

##### Benutzer anzeigen
```
SHOW users
```
##### User löschen
```
DROP USER <username>
```
##### Passwort ändern
```
SET PASSWORD FOR <username> = '<passwort>'
```
##### Admin anlegen
```
CREATE USER <username>  WITH PASSWORD '<passwort>' WITH ALL PRIVILEGES
```
##### Write-User anlegen
Der Benutzer darf nur in eine bestimmte Datenbank schreiben. Wird bei uns von Yanic benutzt.
```
CREATE USER <username>  WITH PASSWORD '<passwort>'
REVOKE ALL PRIVILEGES FROM <username>
GRANT WRITE ON <datenbank> TO <username>
```
Yanic beendet sich nach ein paar Fehlversuchen wenn die Verbindung zu InfluxDB nicht funktioniert.

##### Read-User anlegen
Der Benutzer darf nur aus einer bestimmte Datenbank lesen. Wird bei uns von Grafana benutzt.
```
CREATE USER <username>  WITH PASSWORD '<passwort>'
REVOKE ALL PRIVILEGES FROM <username>
GRANT READ ON <datenbank> TO <username>
```
Grafana fragt in der Weboberfläche nach einem Passwort wenn die Verbindung zu InfluxDB nicht funktioniert.
