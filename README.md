# Mapserver-Docker

Alle für eine Freifunk-Community benötigen Web-Komponenten (Karte, Firmware-Auswahl, Statistiken, Freifunk-API) als Docker-Container für den Betrieb hinter einem Reverse-Proxy.

## Voraussetzungen: ##
- Docker-Compose ist installiert
- Batman-Interface ist auf dem Host als Schnittstelle bat0 verfügbar

## Komponenten: ##
  * Yanic: "Yet another node info collector - for respondd to be used with meshviewer to Grafana (with influxdb or graphite)"
  * InfluxDB: Time-Series-Datenbank für die von Yanic gelieferten Daten
  * Grafana: Zeichnet auf Basis der Daten aus InfluxDB hübsche Diagramme, die in Meshviewer eingebunden werden
  * Meshviewer: Webseite der Freifunk-Map. Bindet Kartentiles von OpenStreetMap ein, von Grafana erzeugten Diagramme und aktuelle Daten von Yanic
  * Firmware: Webseite mit hübscher Oberfläche zur Firmware-Auswahl
  * API: Webseite für Freifunk-API-Datei
  * API-NodeCount-Updater: Knotenanzahl in Freifunk-API-Datei mit Hilfe der nodes.json von Yanic aktuell halten

### Yanic ###
Schickt Broadcasts über das BATMAN-Interface ins Freifunk-Netz, auf die die Access Points antworten mit ihren Node Infos (Anzahl Nutzer, Durchsatz,...)  antworten.
Diese Daten schreibt Yanic für Meshviewer regelmäßig in eine JSON-Datei und zusätzlich für Langzeitstatistiken in die InfluxDB.

Quelle: <https://github.com/FreifunkBremen/yanic>

Läuft im Host-Network weil Zugriff auf bat0 benötigt wird und hat außerdem Zugriff auf den InfluxDB-Container, um dort Daten abzuliefern.

### InfluxDB ###
Time-Series-Datenbank, die von Yanic kontinuierlich mit Daten zu den einzelnen Nodes (Durchsatz, Clientzahl, Laufzeit usw.) befüllt wird. 
Diese Daten werden von Grafana benutzt.

Quelle: influxdb:alpine

Adresse: 192.168.123.6
Ports:
  * 8086 (HTTP-API für Datenbankzugriff)
  * 8088 (RPC für Backuperstellung)

### Grafana ###
Grafana visualisiert die über die Freifunk-Nodes in InfluxDB gesammelten Daten auf einer Webseite.

Es gibt ein Dashboard für Nodes und eines für das Gesamtnetz. Die Dashboards liegen unter grafana/dashboards. Änderungen darin werden sofort (ohne Container-Neustart) übernommen.

Meshviewer nutzt einzelnen Graphen aus den Dashboards, die es im PNG-Format von Grafana zieht.

Quelle: grafana/grafana

Adresse: http://192.168.123.7:3000

### Meshviewer ###
Webseite mit der eigentlichen Kartenansicht des Freifunk-Netzes.

Um die OpenStreetMap-Server zu entlasten ist im Webserver von Meshviewer ein OSM-Tiles-Cache konfiguriert.
Die Meshviewer-Webseite bindet Graphen von Grafana ein.

Die Meshviewer-Webseite wird per Docker gebaut. Der Container bindet das Volume von Yanic ein um auf die Datei "meshviewer.json" zuzugreifen.

Quelle: <https://github.com/ffrgb/meshviewer>

Adresse: http://192.168.123.5:80

### Firmware ###
Webseite mit Firmware-Selector von Freifunk Darmstadt.

Quelle: https://github.com/freifunk-darmstadt/gluon-firmware-selector.git

Adresse: http://192.168.123.8:80

Benötigt Zugriff auf Verzeichnis mit den Firmware-Images.

### Mirror ###
Webseite für die Firmware-Images und Gluon-Pakete.

Adresse: http://192.168.123.10:80

Benötigt Zugriff auf Verzeichnis mit den Firmware-Images und den Gluon-Paketen.

### API ###
Webserver für die Freifunk-API-Datei "api.json".
Die Datei wird von freifunk.net und anderen Diensten gepollt und von API-NodeCount-Updater regelmäßig aktualisiert.

Benötigt für "api.json" Zugriff auf das Volume von API-NodeCount-Updater.

Adresse: http://192.168.123.9:80

### API-NodeCount-Updater ###
Hält Anzahl der aktiven Freifunk-Knoten in der "api.json" (siehe API) aktuell.

Shell-Skript, das regelmäßig die Datei "nodes.json" von Yanic auswertet, daraus die Anzahl der aktiven Knoten ermittelt, und diese in die Datei "api.json" schreibt.

Benötigt für "nodes.json" Zugriff auf das Volume von Yanic.

### Controller ###
Unifi Controller.

Dient zur Konfiguration von Unifi-Geräten mit Unifi-Firmware im Freifunk Client-Netzwerk.

Adresse: 192.168.123.11 bzw. http://10.10.0.10:8080

## Docker-Netzwerk ##
- "host"-Netzwerk: Wird von Yanic für Zugriff auf das Batman-Interface benötigt.
- "gw99_net"-Netzwerk: Bridge-Netzwerk für die Container.
  Jeder Container im "gw99_net"-Netzwerk erhält eine feste IP-Adresse und einen Hostnamen.
  Die Hostnamen werden für die Kommunikation zwischen den Containern benutzt.
  Die festen IP-Adressen können in einem externen Reverse-Proxy verwendet werden, um die Container ins Internet freizugeben.
