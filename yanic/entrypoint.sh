#!/bin/sh

fail () {
    echo "failed!"
    exit 1
}

echo -n "Reconfigure InfluxDB user... "
/usr/bin/envsubst </etc/yanic/config.toml.template >/etc/yanic/config.toml || fail
echo "done."

exec /yanic serve --config /etc/yanic/config.toml
