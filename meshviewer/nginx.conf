user  nginx;
worker_processes  auto;

pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    sendfile        on;
    keepalive_timeout  65;


    ###  Logs anonymisieren: Letzte Stelle durch 0 ersetzen ###
    # normale zugriffe
    map $remote_addr $remote_addr1 {
        default 0.0.0;
        "~(?P<ip>(\d+)\.(\d+)\.(\d+))\.\d+" $ip;
        "~(?P<ip>[^:]+:[^:]+):" $ip;
    }
    map $remote_addr $remote_addr2 {
    default .0;
        "~(?P<ip>(\d+)\.(\d+)\.(\d+))\.\d+" .0;
        "~(?P<ip>[^:]+:[^:]+):" ::;
    }
    map $remote_addr1$remote_addr2 $remote_addr_anon {
        default 0.0.0.0;
        "~(?P<ip>.*)" $ip;
    }

    # reverse proxy (X-Forwarded-For)
    map $http_x_forwarded_for $http_x_forwarded_for_anon1 {
        default 0.0.0;
        "~(?P<ip>(\d+)\.(\d+)\.(\d+))\.\d+" $ip;
        "~(?P<ip>[^:]+:[^:]+):" $ip;
    }
    map $http_x_forwarded_for $http_x_forwarded_for_anon2 {
        default .0;
        "~(?P<ip>(\d+)\.(\d+)\.(\d+))\.\d+" .0;
        "~(?P<ip>[^:]+:[^:]+):" ::;
    }
    map $http_x_forwarded_for_anon1$http_x_forwarded_for_anon2 $http_x_forwarded_for_anon {
        default 0.0.0.0;
        "~(?P<ip>.*)" $ip;
    }

    log_format anonymized '$remote_addr_anon - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '$http_user_agent" "$http_x_forwarded_for_anon"';

    access_log            /var/log/nginx/access.log anonymized;
    error_log             /var/log/nginx/error.log warn;


    ### Verzeichnis für OSM tiles cache, wird automatisch angelegt ###
    proxy_cache_path /var/cache/nginx/tiles_cache levels=1:2 keys_zone=tiles_cache:64m inactive=7d max_size=2G;
    upstream openstreetmap_backend {
        server a.tile.openstreetmap.org;
        server b.tile.openstreetmap.org;
        server c.tile.openstreetmap.org;
    }


    server {
        listen 80;

        # Meshviewer
        location / {
            root /usr/share/nginx/html;
            index index.html index.htm;
        }

        # Yanic mapdata
        location /yanic {
            root /usr/share/nginx/html;
            expires -1;
        }

        # OSM tiles cache
        location /tiles_cache {
            proxy_set_header          X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header          Host tile.openstreetmap.org;
            add_header                X-Cache-Status $upstream_cache_status;

            rewrite                   ^/tiles_cache(/.*)$ $1 break;
            proxy_pass                http://openstreetmap_backend;

            proxy_cache               tiles_cache;
            proxy_cache_valid         202 302 7d;
            proxy_cache_valid         404 1m;
            proxy_cache_lock          on;
            proxy_cache_use_stale     error timeout http_500 http_502 http_503 http_504;
        }
    }

    server {
        listen 80;
        server_name map.meitner.chemnitz.freifunk.net; 

        # Meshviewer
        location / {
            root /usr/share/nginx/html;
            rewrite config.json /ffc.json;
            index index.html index.htm;
        }

        # Yanic mapdata
        location /yanic {
            rewrite /meshviewer.json /yanic/ffc-meshviewer.json;
            root /usr/share/nginx/html;
            expires -1;
        }

        # OSM tiles cache
        location /tiles_cache {
            proxy_set_header          X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header          Host tile.openstreetmap.org;
            add_header                X-Cache-Status $upstream_cache_status;

            rewrite                   ^/tiles_cache(/.*)$ $1 break;
            proxy_pass                http://openstreetmap_backend;

            proxy_cache               tiles_cache;
            proxy_cache_valid         202 302 7d;
            proxy_cache_valid         404 1m;
            proxy_cache_lock          on;
            proxy_cache_use_stale     error timeout http_500 http_502 http_503 http_504;
        }
    }
}
